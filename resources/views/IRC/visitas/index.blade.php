@extends('layouts.app')

@section('content')



    <div class="container" style="border:solid 0px red;padding-top: 80px;">
    <div class="row">
        <div class="col-lg-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Selecione a Loja a qual deseja fazer o Checkin</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>

                    @endif



                        {!! Form::open(['route' => ["produtos.checkin"]]) !!}

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-0">
                                @foreach($auxLoja as $k=>$v)
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="loja_id" value="{{$k}}"> {{$v}}
                                    </label>
                                </div>

                                @endforeach

                            </div>
                        </div>
                        <input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-0">
                                <button type="submit" class="btn btn-primary">
                                    Checkin
                                </button>
                            </div>
                        </div>


                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
