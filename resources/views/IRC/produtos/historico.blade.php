@extends('layouts.app')

@section('content')

    <div class="container" style="border:solid 0px red;padding-top: 80px;">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Histórico de Movimentação do Produto: {{$produto}}</div>

                    <div class="panel-body">




                        <table class="table table-striped gridUm">
                            <thead>
                            <tr>
                                <th class="text-left" style="width: 80px;">Data</th>
                                <th class="text-center" style="width: 80px;">Movimentação</th>
                                <th class="text-left">Cliente / Fornecedor </th>
                                <th class="text-right" style="width: 120px;">Vlr. Unit</th>
                                <th class="text-right" style="width: 120px;">Vlr. Custo</th>
                                <th class="text-right" style="width: 150px;">Qtd. Movimentada</th>
                                <th class="text-right" style="width: 120px;">Saldo</th>

                            </tr>
                            </thead>
                            <tbody>

                            @forelse($objModel as $obj)
                                <tr>
                                    <td class="text-left" >{{$obj['data_mov']}}</td>
                                    <td class="text-center" >{{$obj['movimentacao']}}</td>
                                    <td>cliente</td>
                                    <td>    </td>
                                    <td class="text-right" >{{$obj['custo']}}</td>
                                    <td class="text-right" >{{$obj['qtdMov']}}</td>
                                    <td class="text-right" >{{$obj['saldo']}}</td>

                            {{--        <td class="text-left" >{{$obj->Nome}}</td>
                                    <td class="text-right" >{{$obj->vlr_venda}}</td>
                                    <td class="text-right" >{{$obj->estoque}}</td>--}}
                                </tr>
                            @empty

                                <tr>
                                    <td colspan="4"> Nenhum registro encontrado</td>
                                </tr>
                            @endforelse




                            </tbody>
                            <tfoot>

                            <tr>
                                <th colspan="6" class="text-right" style="border: solid 1px #ffffff;">Saldo Final:</th>
                                <th class="text-right" style="border: solid 1px #ffffff; padding: 5px; background-color: #f1f1f1; vertical-align: middle">
                                   {{$saldo}}
                                </th>
                            </tr>
                            </tfoot>

                        </table>




                    </div>





                </div>
            </div>
        </div>
    </div>
@endsection
