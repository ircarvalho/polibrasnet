@extends('layouts.app')

@section('content')

    <div class="container" style="border:solid 0px red;padding-top: 80px;">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Situação do estoque na:{{$lojaSelecionada->nome}}</div>

                    <div class="panel-body" id="c-estoque">

                        {{-- **************** --}}
                        @include('IRC.produtos.partials.error')
                        {{-- **************** --}}
                        @include('IRC.produtos.partials.success')
                        {{-- **************** --}}

                        <table class="table table-striped gridDois">
                            <thead>
                                <tr>
                                    <th class="text-left" style="width: 220px;">Categoria</th>

                                    <th class="text-left" style="width: 40px;">Cód</th>
                                    <th class="text-left">Nome do produto</th>

                                    <th class="text-right" style="width: 120px;">Vlr Custo</th>
                                    <th class="text-right" style="width: 120px;">Vlr Venda</th>
                                    <th class="text-right" style="width: 120px;">Qtd. Estoque</th>
                                    <th class="text-right" style="width: 80px;">Un Med</th>
                                    <th class="text-right" style="width: 120px;">Custo Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            <span class="hide">
                                {{ $tg = 0 }}
                            </span>

                            @forelse($objModel as $obj)
                                <tr>
                                    <td class="text-left" >{{$obj->categoria}}</td>

                                    <td class="text-left" >{{$obj->cod}}</td>
                                    <td class="text-left" >
                                        <a href="{{ route('produto.historico',['loja'=>$obj->id_loja,'id'=>$obj->id]) }}">
                                        {{$obj->Nome}}
                                        </a>
                                    </td>
                                    <td class="text-right" >{{$obj->vlr_custo}}</td>
                                    <td class="text-right" >{{$obj->vlr_venda}}</td>
                                    <td class="text-right" >{{$obj->estoque}}</td>
                                    <td class="text-right" >{{$obj->sigla}}</td>
                                    <td class="text-right" >{{$obj->total}} </td>
                                </tr>

                                <span class="hide">
                                    {{$tg += $obj->total }}
                                </span>
                            @empty


                            @endforelse

                            </tbody>

                            <tfoot>
                            <tr>
                                <th colspan="7" class="text-right" style="border: solid 1px #ffffff;">Quantidade total em estoque:</th>
                                <th class="text-right" style="padding: 5px; background-color: #f1f1f1; vertical-align: middle">
                                    {{$objModel->sum('estoque')}}
                                </th>
                            </tr>
                            <tr>
                                <th colspan="7" class="text-right" style="border: solid 1px #ffffff;">Custo total em estoque:	:</th>
                                <th class="text-right" style="border: solid 1px #ffffff; padding: 5px; background-color: #f1f1f1; vertical-align: middle">
                                    {{$tg}}
                                </th>
                            </tr>
                            </tfoot>

                        </table>





                    </div>





                </div>
            </div>
        </div>
    </div>

    @include('IRC.produtos.partials.modal-create-orcamento-alimentacao')

@endsection
