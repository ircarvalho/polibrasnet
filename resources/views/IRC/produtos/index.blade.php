@extends('layouts.app')
@section('content')

    <div class="container" style="border:solid 0px red;padding-top: 80px;">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Produtos</div>

                    <div class="panel-body" id="c-produto">

                        {{-- **************** --}}
                        @include('IRC.produtos.partials.success')
                        {{-- **************** --}}

                        <table class="table table-striped gridProdutos">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 20px;">categoria</th>
                                    <th class="text-center" style="width: 20px;">#</th>
                                    <th class="text-left" style="width: 40px;">Cód</th>
                                    <th class="text-left">Nome do produto</th>
                                    <th class="text-left" style="width: 120px;">Valor de Venda</th>
                                    <th class="text-right" style="width: 120px;">Qtd. Estoque</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php

                            $cont = 0 ;

                            @endphp

                            @forelse($objModel as $obj)
                                <tr>
                                    <td class="text-left" >{{$obj->categoria}}</td>
                                    <td class="text-center">{{str_pad($cont++, 4, "0", STR_PAD_LEFT)}}</td>
                                    <td class="text-left" >{{$obj->cod}}</td>

                                    <td class="text-left" >{{$obj->Nome}}</td>
                                    <td class="text-right" >{{$obj->vlr_venda}}</td>
                                    <td class="text-right" >{{$obj->estoque}}</td>
                                </tr>
                            @empty

                                <tr>
                                    <td colspan="4"> Nenhum registro encontrado</td>
                                </tr>
                            @endforelse

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
