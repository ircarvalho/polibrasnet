<div class="modal fade bs-example-modal-sm" id="modal-create-estoque" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
                <h4 class="modal-title">Adicionar Item no Estoque </h4>
            </div>


            {!! Form::open(['route' => ["estoque.store"]]) !!}
                <div class="modal-body">
                    <div class="row">

                        {{-- --}}
                        <div class="form-group">
                            <div class="col-lg-12">
                                {!! Form::label('tipo_mov', 'Movimentação:') !!}
                                {!! Form::select('tipo_mov',['E'=>'E - Entrada'],null, array('required','disabled','class' => 'form-control','id'=>'tipo_mov')) !!}
                            </div>
                        </div>
                        {{-- --}}

                        <div class="form-group">
                            <div class="col-lg-12">
                                {!! Form::label('id_produto', 'Produto') !!}
                                {!! Form::select('id_produto', $auxProduto, null, array('required','class' => 'form-control')) !!}

                            </div>
                        </div>
                        {{-- --}}

                        <div class="form-group">
                            <div class="col-lg-12">
                                {!! Form::label('id_loja_', 'Loja:') !!}
                                {!! Form::select('id_loja_', $auxLoja, $lojaSelecionada->id, array('disabled','required','class' => 'form-control')) !!}
                                {!! Form::hidden("id_loja", $lojaSelecionada->id) !!}

                            </div>
                        </div>
                        {{-- --}}


                        <div class="form-group">
                            <div class="col-lg-12">
                                {!! Form::label('qtd', 'Quantidade:') !!}
                                {!! Form::text("qtd", null, array("class" => "form-control text-right", "placeholder"=>"Informe a quantidade")) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                {!! Form::label('data_mov', 'Data:') !!}
                                {!! Form::text("data_mov", date('d.m.Y'), array("disabled","class" => "form-control text-right", "placeholder"=>"Informe a data")) !!}
                            </div>
                        </div>
                        {{-- --}}

                    </div>
                </div>

            <div class="modal-footer">
                <div class="col-lg-12 text-center">
                    <button type="submit" class="btn btn-success"
                            name="action" value="finished">
                        <i class="fa fa-floppy-o"></i>
                        Salvar
                    </button>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>