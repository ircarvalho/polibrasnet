@if ($errors->any())    
<div class="alert alert-danger alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>

    <ul class="alert">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>    
@endif 

<div class="row">
    <div class="col-sm-12">



        <div class="card-box" style="overflow: hidden">

            <div class="form-group col-lg-12">
                {!! Form::label('Nome', 'Nome do Produto:') !!}
                {!! Form::text("Nome", null, array("class" => "form-control", "placeholder"=>"Informe um nome para o produto")) !!}
            </div>
            

            <div class="form-group col-lg-3">
                {!! Form::label('cod', 'Cód do Produto:') !!}
                {!! Form::text("cod", null, array("class" => "form-control", "placeholder"=>"Preencha um codigo")) !!}
            </div>

            <div class="form-group col-lg-3">
                {!! Form::label('cod_barra', 'Código de Barras (EAN):') !!}
                {!! Form::text("cod_barra", null, array("class" => "form-control", "placeholder"=>"Insira o codigo da unidade")) !!}
            </div>

            <div class="form-group col-lg-4">
                {!! Form::label('id_categoria', 'Categoria do Produto:') !!}
                {!! Form::select('id_categoria', $categorias, 'id_categoria', array('class' => 'form-control')) !!}
            </div>

            <div class="form-group col-lg-2">
                {!! Form::label('id_unidade_medida', 'Unidade de Medida:') !!}
                {!! Form::select('id_unidade_medida', $unMedidas, null, array('class' => 'form-control')) !!}
            </div>



            <div class="form-group col-lg-3">
                {!! Form::label('vlr_venda', 'Valor de Venda:') !!}
                {!! Form::text("vlr_venda", null, array("class" => "form-control", "placeholder"=>"Preencha um vlr para venda")) !!}
            </div>
            <div class="form-group col-lg-2">
                {!! Form::label('vlr_custo', 'Valor de Custo:') !!}
                {!! Form::text("vlr_custo", null, array("class" => "form-control", "placeholder"=>"Preencha um codigo")) !!}
            </div>

            <div class="form-group col-lg-3">
                {!! Form::label('xxxxx', 'Disponível em estoque:') !!}
                {!! Form::text("xxxx", null, array("class" => "form-control", "placeholder"=>"Valor disponível em estoque")) !!}
            </div>

            <div class="form-group col-lg-2">
                {!! Form::label('estoque_min', 'Mínimo em estoque:') !!}
                {!! Form::text("estoque_min", null, array("class" => "form-control", "placeholder"=>"Mínimo em estoque")) !!}
            </div>
            <div class="form-group col-lg-2">
                {!! Form::label('estoque_max', 'Máximo em estoque:') !!}
                {!! Form::text("estoque_max", null, array("class" => "form-control", "placeholder"=>"Máximo em estoque")) !!}
            </div>

            <div class="form-group col-lg-12">
                {!! Form::label('id_fornecedor', 'Fornecedores:') !!}
                {{--{!! Form::text("razao_social", null, array("class" => "form-control", "placeholder"=>"Informe um fornecedor")) !!}--}}
                {!! Form::select('id_fornecedor', $auxFornecedor, 'id_fornecedor', array('class' => 'form-control')) !!}
            </div>
            
        </div>
    </div>
</div>            

<div class="row">

    <div class="col-lg-6 ajuste-btn-cancelar">
        <button type="button" class="btn btn-danger"
                name="action" value="finished"
                onClick="javascript:if(confirm('Você tem certeza que deseja cancelar?.')){location.href = '{{ route('produtos.index') }}';}">
            Cancelar
        </button>
    </div>
    <div class="col-lg-6  text-right ajuste-btn-salvar">
        <button type="submit" class="btn btn-success"
                name="action" value="finished">
            <i class="fa fa-floppy-o"></i>
            Salvar
        </button>
    </div>
</div>
