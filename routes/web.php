<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('visita');





Route::group(['prefix' => 'produto'], function () {

    Route::get('/', ['as' => 'produtos.index', 'uses' => 'IRC\ProdutoController@getlistAllProducts']);
    Route::get('produtos', ['as' => 'lista.produto', 'uses' => 'IRC\ProdutoController@getProdutos']);
    Route::get('create', ['as' => 'produtos.create', 'uses' => 'IRC\ProdutoController@create']);
    Route::post('store', ['as' => 'produtos.store', 'uses' => 'IRC\ProdutoController@store']);


  //  Route::get('/estoque', ['as' => 'produtos.estoque', 'uses' => 'IRC\ProdutoController@getSituacaoEstoque']);

    Route::get('/estoque/loja/{loja}', ['as' => 'estoque.loja', 'uses' => 'IRC\ProdutoController@getSituacaoEstoque']);

    Route::get('{idproduto}/loja/{loja}', ['as' => 'produto.historico', 'uses' => 'IRC\ProdutoController@historicoDeMovimentacaoDoProduto']);



    Route::post('store', ['as' => 'produtos.checkin', 'uses' => 'IRC\ProdutoController@checkin']);

});


Route::group(['prefix' => 'estoque'], function () {

    Route::post('store', ['as' => 'estoque.store', 'uses' => 'IRC\EstoqueController@AdicionarprodutoNoEstoque']);


/*    Route::get('/', ['as' => 'lista-estoque', 'uses' => 'EstoqueController@index']);

    Route::get('index', ['as' => 'estoque.index', 'uses' => 'EstoqueController@index']);



    // Relatorio de vendas do dia
    Route::get('relatorio/loja', ['as' => 'relatorio.vendas.dia', 'uses' => 'EstoqueController@vendasDoDia']);
    Route::get('{id}/destroy', ['as' => 'estoque.destroy', 'uses' => 'EstoqueController@destroy']);
    */

});

