$(document).ready(function() {

    $('.estoque').DataTable({

        // responsive: true,
         dom: '<"html5buttons"B>lTfgitp', buttons: ['copy', 'csv', 'excel', 'pdf','print'],
        "oLanguage": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ registros por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },

        'bInfo': true,
        'bPaginate': true,
        "searching": true,
        "aoColumns":[
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false}
        ],

        "columnDefs": [
            { "visible": false, "targets": 0 }
        ],
        // "order": [[ 1, 'desc' ]],
        "displayLength": 100,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="7">'+group+'</td></tr>'
                    );

                    last = group;
                }
            } );
        }
    } );



    $('.gridUm').DataTable({
         dom: '<"html5buttons"B>lTfgitp',
      //  dom: '<"html5buttons"B>lTfgitp', buttons: ['copy', 'csv', 'excel', 'pdf','print'],
        "oLanguage": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ registros por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Listagem'},
            {extend: 'pdf', title: 'Listagem'}  ,
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('red-bg');
                    $(win.document.body).css('font-size', '18px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }

        ]
    });

    //-------------

    $('.gridDois').DataTable({

        // responsive: true,
        dom: '<"html5buttons"B>lTfgitp', buttons: ['copy', 'csv', 'excel', 'pdf','print'],
        "oLanguage": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ registros por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },

        'bInfo': true,
        'bPaginate': true,
        "searching": true,
        "aoColumns":[
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false}
        ],

        "columnDefs": [
            { "visible": false, "targets": 0 }
        ],
        // "order": [[ 1, 'desc' ]],
        "displayLength": 10,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="8"><b>'+group+'</b></td></tr>'
                    );

                    last = group;
                }
            } );
        }
    } );


    $('.gridProdutos').DataTable({

        // responsive: true,
        dom: '<"html5buttons"B>lTfgitp', buttons: ['copy', 'csv', 'excel', 'pdf','print'],
        "oLanguage": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ registros por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },

        'bInfo': true,
        'bPaginate': true,
        "searching": true,
        "aoColumns":[
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false}
            ],

        "columnDefs": [
            { "visible": false, "targets": 0 }
        ],
        // "order": [[ 1, 'desc' ]],
        "displayLength": 10,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="8"><b>'+group+'</b></td></tr>'
                    );

                    last = group;
                }
            } );
        }
    } );

    //---


    //---

    var btnAddProduto = '<a class="btn btn-success" '+' href="produto/create" '+' title="Cadastrar Produto"><span><i class="glyphicon glyphicon-list-alt"></i> &nbsp;[ + ]</span></a>';
    $("#c-produto").find(".dt-buttons.btn-group").prepend(btnAddProduto);
    //----
    //var btnAddEstoque = '<a class="btn btn-success" '+' href="produto/create" '+' title="Cadastrar Estoque"><span><i class="glyphicon glyphicon-list-alt"></i> &nbsp;[ + ]</span></a>';

    var btnAddEstoque = '<a class="btn btn-danger" ' +
        'tabindex="0" ' +
        'aria-controls="DataTables_Table_0" ' +
        'data-toggle="modal" ' +
        'data-target="#modal-create-estoque" ' +
        'name="action" ' +
        'value="finished" ' +
        'title="Adicionar Item no Estoque"><span><i class="fa fa-money"></i> &nbsp;[ + ]</span></a>';

    $("#c-estoque").find(".dt-buttons.btn-group").prepend(btnAddEstoque);
    //

});

