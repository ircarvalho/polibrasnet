<?php

namespace App\IRC\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{

    public    $timestamps = false;
    protected $fillable = array(
        'Nome',
        'id_categoria',
        'vlr_custo',
        'vlr_venda',
        'estoque_min',
        'estoque_max',
        'id_unidade_medida',
        'peso',
        'id_fornecedor',
        'descricao',
        'cod',
        'cod_barra'
    );

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'id_categoria');
    }

    public function loja()
    {
        return $this->belongsToMany(Loja::class, 'estoque_loja')->withPivot('qtd');
    }


}
