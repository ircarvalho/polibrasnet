<?php

namespace App\IRC\Models;

use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{

   // protected $table = 'fornecedores';
    // protected $primaryKey = 'id';

    public    $timestamps = false;
    protected $fillable = array(
        'nome',
        'checkin',
        'checkout',
        'loja_id',
        'user_id'
    );

}
