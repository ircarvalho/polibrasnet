<?php

namespace App\IRC\Models;

use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{

    protected $table = 'fornecedores';
    // protected $primaryKey = 'id';

    public    $timestamps = false;
    protected $fillable = array(
        'nome',
        'id_cidade'
    );

}
