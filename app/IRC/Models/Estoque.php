<?php

namespace App\IRC\Models;

use Illuminate\Database\Eloquent\Model;

class Estoque extends Model
{
//    protected $table = 'estoqueGeral';
    protected $table = 'estoque';
    protected $primaryKey = 'id';

    public    $timestamps = false;
    protected $fillable = array(
        'data_mov',
        'tipo_mov',
        'id_produto',
        'qtd',
        'id_loja'
    );

}
