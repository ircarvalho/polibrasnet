<?php

namespace App\IRC\Repositories;

use App\IRC\Models\Fornecedor;

class FornecedorRepository extends BaseRepository
{
    protected $modelClass = Fornecedor::class;

}