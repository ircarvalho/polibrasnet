<?php

namespace App\IRC\Repositories;

use App\IRC\Models\Produto;
use Illuminate\Support\Facades\DB;

class ProdutoRepository extends BaseRepository
{

    protected $modelClass = Produto::class;


    /**
     * @param int $limit
     * @param bool $paginate
     * @return Paginator|\Illuminate\Database\Eloquent\Collection
     *
     */
    public function getListaProd($limit = 100, $paginate = true)
    {
        $query = $this->newQuery();
        $query->select('produtos.id', 'produtos.cod', 'produtos.Nome', 'produtos.vlr_venda','cat.nome as categoria',
                        DB::raw('COALESCE(SUM(eg.creditoDia) - SUM(eg.debitoDia),0) as estoque'));

        $query->leftJoin('estoqueGeral as eg', 'eg.id_produto', '=', 'produtos.id');
        $query->leftJoin('categorias as cat', 'cat.id', '=', 'produtos.id_categoria');
        $query->groupBy('produtos.id', 'produtos.cod', 'produtos.Nome', 'produtos.vlr_venda','cat.nome');
        $query->orderBy('produtos.Nome','asc');

        return $this->doQuery($query, $limit, $paginate);
    }


    /**
     * @param int $limit
     * @param bool $paginate
     * @param null $loja
     * @return Paginator|\Illuminate\Database\Eloquent\Collection
     *
     */
    public function getSituacaoEstoque($loja = NULL)
    {

       // echo "--->" . $loja; exit;

        $query = $this->newQuery();
        $query->select('produtos.id', 'produtos.cod', 'produtos.Nome', 'produtos.vlr_custo','produtos.vlr_venda','eg.sigla','eg.categoria','eg.id_loja',
                        DB::raw('(SUM(creditoDia) - SUM(debitoDia)) as estoque'),
                        DB::raw('((SUM(creditoDia) - SUM(debitoDia) ) *  produtos.vlr_custo )AS total'));

        $query->Join('estoqueGeral as eg', 'eg.id_produto', '=', 'produtos.id');
        $query->orderBy('produtos.Nome','asc');

        if($loja)
        {
            $query->where('eg.id_loja',$loja);
        }
        $query->groupBy('produtos.id', 'produtos.cod', 'produtos.Nome', 'produtos.vlr_custo','produtos.vlr_venda','eg.sigla','eg.categoria','eg.id_loja');

        return $this->doQuery($query,100,true);
    }


    /**
     * @param $idLoja
     * @param $idProduto
     * @return Paginator|\Illuminate\Database\Eloquent\Collection
     */
    public function getHistoricoDeMovimentacaoDoProduto($idProduto,$idLoja){

        $query = $this->newQuery();
        $query->select('produtos.id', 'produtos.cod', 'produto', 'produtos.vlr_venda','produtos.vlr_custo','creditoDia','debitoDia','data_mov','tipo_mov');
        $query->Join('estoqueGeral as eg', 'eg.id_produto', '=', 'produtos.id');
        //$query->groupBy('produtos.id');
        $query->groupBy('produtos.id', 'produtos.cod', 'produto', 'produtos.vlr_venda','produtos.vlr_custo','creditoDia','debitoDia','data_mov','tipo_mov');
        $query->orderBy('data_mov','asc');

        $query->where('produtos.id',$idProduto);
        $query->where('id_loja',$idLoja);





/*
        if($idLoja)
        {
            $query->where('id_loja',$idLoja);

        }
*/
      // echo "produto: $idProduto <br/>"
       // echo $query->toSql();
   // exit;


        return $this->doQuery($query);

    }

}