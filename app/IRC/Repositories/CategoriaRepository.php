<?php

namespace App\IRC\Repositories;

use App\IRC\Models\Categoria;

class CategoriaRepository extends BaseRepository
{
    protected $modelClass = Categoria::class;

}