<?php

namespace App\IRC\Repositories;

use App\IRC\Models\Visita;

class VisitaRepository extends BaseRepository
{
    protected $modelClass = Visita::class;

}