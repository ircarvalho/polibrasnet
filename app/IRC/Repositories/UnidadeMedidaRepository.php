<?php

namespace App\IRC\Repositories;

use App\IRC\Models\UnidadeMedida;

class UnidadeMedidaRepository extends BaseRepository
{
    protected $modelClass = UnidadeMedida::class;

}