<?php

namespace App\IRC\Repositories;

use App\IRC\Models\Estoque;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EstoqueRepository extends BaseRepository
{
    protected $modelClass = Estoque::class;

/*

    public function getListaEstoque($limit = 50, $paginate = true, $loja = NULL)
    {
        $query = $this->newQuery();
        $query->select('cod', 'produto', 'sigla', 'categoria', 'vlr_custo', 'vlr_venda', 'id_produto', 'id_loja',
                        DB::raw('(SUM(creditoDia) - SUM(debitoDia)) as estoque'), DB::raw('((SUM(creditoDia) - SUM(debitoDia) ) *  vlr_custo )AS total'));

        if(is_array($loja))
        {
           $query->whereIn('id_loja',$loja);

        }

        $query->groupBy('id_produto');
        $query->groupBy('id_loja');


        return $this->doQuery($query,$limit,$paginate);
    }


    public function getHistoricoDeMovimentacaoDoProduto($idLoja, $idProduto){

        $query = $this->newQuery();
        $query->select('data_mov','tipo_mov','vlr_custo', 'vlr_venda','creditoDia','debitoDia','produto', 'id_loja','produto');
        $query->orderBy('id', 'asc');

        if($idProduto)
        {
            $query->where('id_produto',$idProduto);

        }
        if($idLoja)
        {
            $query->where('id_loja',$idLoja);

        }

        return $this->doQuery($query);

    }

    public function getVendasDoDia($data_i, $data_f,$idLoja){



        $query = $this->newQuery();
        $query->select('data_mov','tipo_mov','vlr_custo', 'vlr_venda','creditoDia','debitoDia','produto', 'id_loja','produto');
        $query->orderBy('id', 'asc');
        $query->where('tipo_mov','=','S');

        if($idLoja)
        {
            $query->where('id_loja',$idLoja);

        }


        if ($data_i) {
            $auxData_i = Carbon::parse($data_i)->format('Y-m-d');
            $query->where('data_mov','>=', $auxData_i);
        } else {
            $auxData_i = Carbon::now()->format('Y-m-d');
            $query->where('data_mov','>=',$auxData_i);
        }

        if ($data_f) {
            $auxData_f = Carbon::parse($data_f)->format('Y-m-d');
            $query->where('data_mov','<=',$auxData_f);
        } else {
            $auxData_f = Carbon::now()->format('Y-m-d');
            $query->where('data_mov','=',$auxData_f);
            // $query->whereRaw("DATE(data_mov) = '$auxData_f'");
        }


     //  echo $query->toSql();
      // exit;

//        if($idLoja)
//        {
//            $query->where('id_loja',$idLoja);
//
//        }

        return $this->doQuery($query);

    }
*/



}