<?php

namespace App\IRC\Repositories;


use App\IRC\Models\Loja;

class LojaRepository extends BaseRepository
{
    protected $modelClass = Loja::class;

}