<?php

namespace App\Http\Controllers\IRC;

use App\Http\Controllers\Controller;
use App\IRC\Models\Produto;
use App\IRC\Models\Visita;
use App\IRC\Repositories\CategoriaRepository;
use App\IRC\Repositories\EstoqueRepository;
use App\IRC\Repositories\FornecedorRepository;
use App\IRC\Repositories\LojaRepository;
use App\IRC\Repositories\ProdutoRepository;
use App\IRC\Repositories\UnidadeMedidaRepository;
use App\IRC\Repositories\VisitaRepository;
use Illuminate\Http\Request;
use Mockery\Exception;
use Validator;

class ProdutoController extends Controller
{


    protected $path = 'IRC.produtos';

    private $estoqueRepository;
    private $categoriaRepository;
    private $produtoRepository;
    private $unidadeMedida;
    private $lojaRepository;
    private $fornecedorRepository;


    public function __construct(EstoqueRepository $estoqueRepository,
                                CategoriaRepository $categoriaRepository,
                                UnidadeMedidaRepository $unidadeMedidaRepository,
                                ProdutoRepository $produtoRepository,
                                FornecedorRepository $fornecedorRepository,
                                LojaRepository $lojaRepository
    )
    {
        $this->middleware('auth');


        $this->estoqueRepository = $estoqueRepository;
        $this->categoriaRepository = $categoriaRepository;
        $this->produtoRepository = $produtoRepository;
        $this->lojaRepository = $lojaRepository;
        $this->unidadeMedida = $unidadeMedidaRepository;
        $this->fornecedorRepository = $fornecedorRepository;

    }


    /**
     * Carrega tela / form para inserção de um novo Produto
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        $categorias = $this->categoriaRepository->getAll()->pluck('nome', 'id');
        $unMedidas = $this->unidadeMedida->getAll()->pluck('sigla', 'id');
        $auxLoja = $this->lojaRepository->getAll()->pluck('nome', 'id');
        $auxFornecedor = $this->fornecedorRepository->getAll()->pluck('nome', 'id');


        return view($this->path . '.create', compact('categorias', 'unMedidas','auxLoja','auxFornecedor'));
    }


    /**
     * Carrega um lista com todos os produtos cadastrados
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getlistAllProducts()
    {


        $objModel = $this->produtoRepository->getListaProd();
        $auxLoja = $this->lojaRepository->getAll()->pluck('nome', 'id');

        return view($this->path . '.index', compact('objModel','auxLoja'));

    }


    public function getHTMLSelectListaDeProdutos()
    {

        return $this->produtoRepository->getListaProd()->pluck('Nome', 'id');
    }


    /**
     * Carrega o estoque de cada loja
     * @param int $loja identificação da loja
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function getSituacaoEstoque($loja)
    {
        $loja = ($loja > 0) ? $loja : null;
        try {

            $auxProduto = $this->getHTMLSelectListaDeProdutos();
            $auxLoja = $this->lojaRepository->getAll()->pluck('nome', 'id');
            $lojaSelecionada = $this->lojaRepository->findByID($loja);

            $objModel = $this->produtoRepository->getSituacaoEstoque($loja);

        } catch (Exception $e) {

            return response()->view('errors.error', ['error' => $e->getMessage()], 500);

        }

        return view($this->path . '.estoque', compact('objModel', 'auxProduto','auxLoja','lojaSelecionada'));

    }


    /**
     *
     * @param $idProduto
     * @param $idLoja
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function historicoDeMovimentacaoDoProduto($idProduto, $idLoja)
    {


        $objModel = $this->produtoRepository->getHistoricoDeMovimentacaoDoProduto($idProduto, $idLoja);

        //$qtdMov = 0;
        $novoArr = array();
        $saldo = 0;

        try {

            $auxLoja = $this->lojaRepository->getAll()->pluck('nome', 'id');

            foreach ($objModel as $obj) {

                $qtdMov = $obj->creditoDia;
                $saldo = $saldo + $obj->creditoDia - $obj->debitoDia;

                if ($obj->creditoDia == 0) {
                    $qtdMov = $obj->debitoDia;
                }


                $novoArr[] =

                    [
                        'data_mov' => $obj->data_mov,
                        'movimentacao' => $obj->tipo_mov,
                        'creditoDia' => $obj->creditoDia,
                        'debitoDia' => $obj->debitoDia,
                        'qtdMov' => $qtdMov,
                        'venda' => $obj->vlr_venda,
                        'custo' => $obj->vlr_custo,
                        'saldo' => $saldo,
                        'produto' => $obj->produto

                    ];


            }

            $produto = $novoArr[0]['produto'];

        } catch (\Exception $e) {

            return response()->view('errors.error', ['error' => $e->getMessage()], 500);


        }


        return view($this->path . '.historico', ['objModel' => $novoArr, 'produto' => $produto, 'saldo' => $saldo,'auxLoja'=>$auxLoja]);

    }


    /*
    public function historicoDeMovimentacaoDoproduto(EstoqueRepository $estoqueRepository, LojaRepository $lojaRepository, $idLoja, $idProduto){


        $objModel = $estoqueRepository->getHistoricoDeMovimentacaoDoProduto($idLoja, $idProduto);

        $qtdMov = 0;
        $novoArr = array();
        $saldo = 0;

        foreach ($objModel as $obj){

            $qtdMov = $obj->creditoDia ;
            $saldo = $saldo + $obj->creditoDia -$obj->debitoDia ;

            if($obj->creditoDia == 0){
                $qtdMov = $obj->debitoDia;
            }

            $novoArr[] = [
                'data_mov'=> $obj->data_mov,
                'movimentacao'=> $obj->tipo_mov,
                'qtdMov'=>$qtdMov,
                'saldo'=>$saldo,
                'produto'=>$obj->produto,


            ];

        }

        //$produto = array_key_exists($novoArr[0]['produto']) ? $novoArr[0]['produto'] : 1;

        //echo $produto;

        // $produto = (!isset($variavel)) ? 'valor padrão' : $variavel;
        $produto = $novoArr[0]['produto'] ?? "dfsdfsdfsd";


        //  echo "<pre>";
        //  print_r($novoArr);
        //  exit;

        return view($this->path . '.historico',['objModel' => $novoArr,'produto'=>$produto,'saldo'=>$saldo]);

    }
*/


    public function getProdutos(LojaRepositoryXXXXX $lojaRepository)
    {

        $loja = \Input::get('id_loja');

        $lojas = $lojaRepository->getAll()->pluck('nome', 'id');


        $objModel = $this->estoqueRepository->getListaEstoque(100, true, $loja);

        return view($this->path . '.index', ['objModel' => $objModel, 'lojas' => $lojas]);

    }


    /**
     * Salva um novo produto no banco
     * @param Request $request
     * @param ProdutoRepository $produtoRepository
     * @return $this
     */
    public function store(Request $request, ProdutoRepository $produtoRepository)
    {

        $dadosForm = $request->all();


        $rules = [
            'Nome' => 'required|min:6'
            /*,
            'sigla' => 'required|min:3',
            'tipo' => 'required',
            'razao_social' => 'required|min:4'
            */
        ];


        $messages = [
            'Nome.required' => 'O campo NOME é obrigatório.',
            'Nome.min' => 'O campo NOME deve ter pelo menos :min caracteres.'
            /*,
            'sigla.required' => 'O campo SIGLA é obrigatório.',
            'sigla.min' => 'O campo SIGLA deve ter pelo menos :min caracteres.',
            'tipo.required' => 'O campo TIPO é obrigatório.',
            'razao_social.required' => 'O campo RAZÃO SOCIAL é obrigatório.'
            */
        ];
        $validator = Validator::make($dadosForm, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('produtos.create')
                ->withErrors($validator)
                ->withInput();
        }


        $objProduto = Produto::create($request->all());

        return redirect()
            ->route('produtos.index')
            ->withSuccess('Salvo com sucesso');

    }


    public function checkin(Request $request, VisitaRepository $visitaRepository)
    {

        //$dadosForm = $request->all()->id_loja;

        $dadosForm = [];

        $dadosForm['checkin'] = date('Y-m-d h:m:s');
        $dadosForm['loja_id'] = $request->input('loja_id');
        $dadosForm['user_id'] = $request->input('user_id');

        $rules = [
            'loja_id' => 'required'
            /*,
            'sigla' => 'required|min:3',
            'tipo' => 'required',
            'razao_social' => 'required|min:4'
            */
        ];

      //  echo "<pre>";

        //print_r($dadosForm);

        //exit;

        $messages = [
            'loja_id.required' => 'O campo é obrigatório.'
        ];
        $validator = Validator::make($dadosForm, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('produtos.create')
                ->withErrors($validator)
                ->withInput();
        }


        $objProduto = Visita::create($dadosForm);

        return redirect()->route('estoque.loja', ['loja' => $request->input('loja_id')])->withSuccess('Entrada Registrada');

       /* return redirect()
            ->route('produtos.index')
            ->withSuccess('Salvo com sucesso');*/

    }

}
