<?php

namespace App\Http\Controllers\IRC;

use App\Http\Controllers\Controller;
use App\IRC\Models\Estoque;
use App\IRC\Repositories\EstoqueRepository;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Validator;

use Validator;


class EstoqueController extends Controller
{

    protected $path = 'estoque';


    /**
     * @param EstoqueRepository $estoqueRepository
     * @param LojaRepository $lojaRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function indexccc(EstoqueRepository $estoqueRepository, LojaRepository $lojaRepository) {

        $loja = \Input::get('id_loja');

        $objModel = $estoqueRepository->getListaEstoque(100,true, $loja);
        $lojas = $lojaRepository->getAll()->pluck('nome','id');

        return view($this->path . '.index',['objModel' => $objModel,'lojas'=>$lojas]);

    }


/*    public function historicoDeMovimentacaoDoproduto(EstoqueRepository $estoqueRepository, LojaRepository $lojaRepository, $idLoja, $idProduto){


        $objModel = $estoqueRepository->getHistoricoDeMovimentacaoDoProduto($idLoja, $idProduto);

        $qtdMov = 0;
        $novoArr = array();
        $saldo = 0;

        foreach ($objModel as $obj){

               $qtdMov = $obj->creditoDia ;
               $saldo = $saldo + $obj->creditoDia -$obj->debitoDia ;

            if($obj->creditoDia == 0){
               $qtdMov = $obj->debitoDia;
            }

            $novoArr[] = [
                'data_mov'=> $obj->data_mov,
                'movimentacao'=> $obj->tipo_mov,
                'qtdMov'=>$qtdMov,
                'saldo'=>$saldo,
                'produto'=>$obj->produto,


            ];

        }

         //$produto = array_key_exists($novoArr[0]['produto']) ? $novoArr[0]['produto'] : 1;

        //echo $produto;

       // $produto = (!isset($variavel)) ? 'valor padrão' : $variavel;
         $produto = $novoArr[0]['produto'] ?? "dfsdfsdfsd";


      //  echo "<pre>";
      //  print_r($novoArr);
      //  exit;

        return view($this->path . '.historico',['objModel' => $novoArr,'produto'=>$produto,'saldo'=>$saldo]);

    }
*/



/*    public function getProdutos(EstoqueRepository $estoqueRepository, LojaRepository $lojaRepository) {

        $loja = \Input::get('id_loja');
        $lojas = $lojaRepository->getAll()->pluck('nome','id');

        $objModel = $estoqueRepository->getListaEstoque(100,true,$loja);

        return view($this->path . '.produtos',['objModel' => $objModel,'lojas'=>$lojas]);

    }*/





    //

/*    verificar
    public function vendasDoDia(EstoqueRepository $estoqueRepository, LojaRepository $lojaRepository){




        $qtdMov = 0;
        $novoArr = array();
        $saldo = 0;

        $data_i = \Input::get('data_i');
        $data_f = \Input::get('data_f');

        $loja = \Input::get('id_loja');
        $lojas = $lojaRepository->getAll()->pluck('nome','id');

        $objModel = $estoqueRepository->getVendasDoDia($data_i,$data_f, $loja);



        foreach ($objModel as $obj){

            $qtdMov = $obj->creditoDia ;
            $saldo = $saldo + $obj->creditoDia -$obj->debitoDia ;

            if($obj->creditoDia == 0){
                $qtdMov = $obj->debitoDia;
            }

            $novoArr[] = [
                'data_mov'=> $obj->data_mov,
                'movimentacao'=> $obj->tipo_mov,
                'qtdMov'=>$qtdMov,
                'saldo'=>$saldo,
                'id_loja'=>$obj->id_loja,
                'produto'=>$obj->produto,


            ];

        }

        $produto = $novoArr[0]['produto'];
       //  $produto = $novoArr[0]['produto'] ? "0";


        //  echo "<pre>";
        //  print_r($novoArr);
        //  exit;

        return view($this->path . '.relatorioDeVendasDoDia',[
            'objModel' => $novoArr,
            'produto'=>$produto,
            'saldo'=>$saldo,
            'lojas'=>$lojas,
            'data_i'=>$data_i,
            'data_f'=>$data_f,

        ]);

    }
*/



/*    public function createxxsssssssssssss() {
        return view($this->path . '.create');
    }*/


/*    public function storexx(Request $request)
    {

        $dadosForm = $request->all();
        $rules = [
            'nome' => 'required|min:6',
            'sigla' => 'required|min:3',
            'tipo' => 'required',
            'razao_social' => 'required|min:4'
        ];

        $messages = [
            'nome.required' => 'O campo NOME é obrigatório.',
            'nome.min' => 'O campo NOME deve ter pelo menos :min caracteres.',
            'sigla.required' => 'O campo SIGLA é obrigatório.',
            'sigla.min' => 'O campo SIGLA deve ter pelo menos :min caracteres.',
            'tipo.required' => 'O campo TIPO é obrigatório.',
            'razao_social.required' => 'O campo RAZÃO SOCIAL é obrigatório.'
        ];
        $validator = Validator::make($dadosForm,$rules,$messages );

        if($validator->fails()){
            return redirect()->route($this->path . '.create')
                ->withErrors($validator)
                ->withInput();
        }

        $objTipoEspaco = $this->getModel()->create($request->all());

        return redirect()
            ->route($this->path .'.index')
            ->withSuccess('Salvo com sucesso');

    }*/


    /**
     * @param Request $request
     * @param EstoqueRepository $estoqueRepository
     * @return $this
     */
    public function AdicionarprodutoNoEstoque(Request $request, EstoqueRepository $estoqueRepository)
    {



        $dadosForm = $request->all();
        $dadosForm['tipo_mov'] = 'E';
        $dadosForm['data_mov'] = date('Y-m-d');
        $loja = $dadosForm['id_loja'] ;

        $rules = [
            'qtd' => 'required'

        ];

        $messages = [
            'qtd.required' => 'O campo é obrigatório.'
        ];

        $validator = Validator::make($dadosForm, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('estoque.loja', ['loja' => $loja])
                ->withErrors($validator)
                ->withInput();
        }

        Estoque::create($dadosForm);

        return redirect()->route('estoque.loja', ['loja' => $loja])->withSuccess('Produto adicionado com sucesso');

    }


}