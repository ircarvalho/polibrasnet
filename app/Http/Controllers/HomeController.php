<?php

namespace App\Http\Controllers;

use App\Http\Controllers\IRC\BaseController;
use App\IRC\Repositories\LojaRepository;
use Illuminate\Http\Request;

class HomeController extends BaseController
{

    private $lojaRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LojaRepository $lojaRepository)
    {
        $this->middleware('auth');
        $this->lojaRepository = $lojaRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       // echo "aqui";
       // exit;

        $auxLoja = $this->lojaRepository->getAll()->pluck('nome', 'id');

        return view('IRC/visitas/index',compact('auxLoja'));
    }



}
